import { Pharmacy } from "../components/pharmacy";
import { Drug } from "../components/drug";

describe("Pharmacy", () => {
  it("should decrease the benefit and expiresIn", () => {
    expect(new Pharmacy([new Drug("test", 2, 3)]).updateBenefitValue()).toEqual(
      [new Drug("test", 1, 2)]
    );
  });
  it("should return an empty array", () => {
    expect(new Pharmacy([]).updateBenefitValue()).toEqual([]);
  });
});
