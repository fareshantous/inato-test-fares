import { Drug } from "../components/drug";

describe("Drug", () => {
  it("should decrease the benefit and expiresIn", () => {
    const drug = new Drug("test", 2, 3);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("test", 1, 2));
  });
  // Once the expiration date has passed, Benefit degrades twice as fast.
  it("should decrease the benefit twice as fast if expiresIn < 0", () => {
    const drug = new Drug("test", -2, 3);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("test", -3, 1));
  });
  // "Herbal Tea" actually increases in Benefit the older it gets. Benefit increases twice as fast after the expiration date.
  it("should increase the benefit and decrease expiresIn", () => {
    const drug = new Drug("Herbal Tea", 2, 3);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Herbal Tea", 1, 4));
  });
  it("should increase the benefit twice as fast if expiresIn <= 0", () => {
    const drug = new Drug("Herbal Tea", -2, 3);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Herbal Tea", -3, 5));
  });
  it("should increase the benefit twice as fast if expiresIn <= 0", () => {
    const drug = new Drug("Herbal Tea", 0, 3);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Herbal Tea", -1, 5));
  });
  // "Magic Pill" never expires nor decreases in Benefit.
  it("should decrease the benefit and expiresIn", () => {
    const drug = new Drug("Magic Pill", 2, 3);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Magic Pill", 2, 3));
  });
  it("should decrease the benefit and expiresIn", () => {
    const drug = new Drug("Magic Pill", -2, 3);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Magic Pill", -2, 3));
  });
  // "Fervex", like Herbal Tea, increases in Benefit as its expiration date approaches.
  // Benefit increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but Benefit drops to 0 after the expiration date.
  it("should increase the benefit and decrease expiresIn", () => {
    const drug = new Drug("Fervex", 20, 10);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Fervex", 19, 11));
  });
  it("should increase the benefit by 2 if expiresIn <= 10 and >5", () => {
    const drug = new Drug("Fervex", 8, 10);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Fervex", 7, 12));
  });
  it("should increase the benefit by 3 if expiresIn <= 5 and > 0", () => {
    const drug = new Drug("Fervex", 2, 10);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Fervex", 1, 13));
  });
  it("should drop the benefit to 0 if expiresIn < 0", () => {
    const drug = new Drug("Fervex", -1, 10);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Fervex", -2, 0));
  });
  it("should increase the benefit by 3, but never more than 50", () => {
    const drug = new Drug("Fervex", 2, 49);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Fervex", 1, 50));
  });
  // "Dafalgan" degrades in Benefit twice as fast as normal drugs.
  it("should decrease the benefit by 2 and decrease expiresIn", () => {
    const drug = new Drug("Dafalgan", 2, 3);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Dafalgan", 1, 1));
  });
  it("should decrease the benefit by 4 if expiresIn <= 0", () => {
    const drug = new Drug("Dafalgan", -2, 10);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Dafalgan", -3, 6));
  });
  it("should decrease the benefit but the benefit is always >= 0", () => {
    const drug = new Drug("Dafalgan", -2, 3);
    drug.updateBenefitValueOfOneDrug();
    expect(drug).toEqual(new Drug("Dafalgan", -3, 0));
  });
});
