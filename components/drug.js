export class Drug {
  constructor(name, expiresIn, benefit) {
    this.name = name;
    this.expiresIn = expiresIn;
    this.benefit = benefit;
  }

  updateBenefitValueOfOneDrug() {
    this.updateBenefit();
    this.updateExpiresIn();
  }

  updateBenefit() {
    const expired = this.expiresIn < 1;
    const normalDrugBenefitDecreaseDelta = expired ? 2 : 1;
    const dafalganBenefitDecreaseDelta = 2 * normalDrugBenefitDecreaseDelta;
    if (this.name == "Fervex") {
      if (this.expiresIn >= 11) {
        this.benefit = this.increaseBenefit(this.benefit, 1);
      } else if (this.expiresIn >= 6 && this.expiresIn < 11) {
        this.benefit = this.increaseBenefit(this.benefit, 2);
      } else if (!expired && this.expiresIn < 6) {
        this.benefit = this.increaseBenefit(this.benefit, 3);
      } else {
        this.benefit = 0;
      }
    } else if (this.name == "Herbal Tea") {
      this.benefit = this.increaseBenefit(this.benefit, expired ? 2 : 1);
    } else if (this.name == "Magic Pill") {
      // do nothing
    } else if (this.name == "Dafalgan") {
      this.benefit = this.decreaseBenefit(
        this.benefit,
        dafalganBenefitDecreaseDelta
      );
    } else {
      this.benefit = this.decreaseBenefit(
        this.benefit,
        normalDrugBenefitDecreaseDelta
      );
    }
  }

  increaseBenefit(benefit, delta) {
    const maxBenefit = 50;
    return Math.min(benefit + delta, maxBenefit);
  }

  decreaseBenefit(benefit, delta) {
    const minBenefit = 0;
    return Math.max(benefit - delta, minBenefit);
  }

  updateExpiresIn() {
    if (this.name != "Magic Pill") {
      this.expiresIn = this.expiresIn - 1;
    }
  }
}
